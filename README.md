# Docker Image

This projects creates a Docker image, but does not make deployments using the image. This is useful for creating images that are extended or deployed by other projects.

## What is a Docker Image?

An image is an executable package that includes everything needed to run an application — the code, a runtime, libraries, environment variables, and configuration files. Images are used to launch containers. See [Get Started (Docker Documentation)](https://docs.docker.com/get-started/) for general information about Docker.

## Getting Started

Fill the Dockerfile with instructions for your image. See [Dockerfile reference](https://docs.docker.com/engine/reference/builder/) for valid instructions and [Best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/) for recommendations on building efficient images.
